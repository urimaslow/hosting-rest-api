
## Deployment

The application has been dockerized with Dockerfile and docker-compose. For deployment just write the following command on project root: 

> docker-compose up 


---

## Test

In addition to others development tests it has been included BDD tests developed with Gherkin. You can find these in features folder. This command permits to execute them:
> go get github.com/DATA-DOG/godog/cmd/godog  

It has been added an interactive documentation of API description based on Apiary: 
https://urimaslow.docs.apiary.io/# 