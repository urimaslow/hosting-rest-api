package config

import (
	"encoding/json"
	"hosting-rest-api/util"
	"os"
)

var (
	Cfg *Configuration
)

type Configuration struct {
	Host string `json:"host"`
	Port int `json:"port"`
	Database *Database`json:"database"`
}

type Database struct {
	Plugin string `json:"plugin"`
	Source string `json:"source"`
}

func LoadConfiguration() {
	var err error
	file, err := os.Open("application.json")
	util.CheckErr(err)
	defer file.Close()
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&Cfg)
	util.CheckErr(err)
}