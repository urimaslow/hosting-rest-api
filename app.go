package main

import (
	"hosting-rest-api/config"
	"hosting-rest-api/db"
	"hosting-rest-api/handler"
)

func main() {
	config.LoadConfiguration()
	db.InitDatabase()
	handler.InitHandlers()
}