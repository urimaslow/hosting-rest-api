package model

import (
	"database/sql"
	"fmt"
)

type Hosting struct {
	Id int `json:"id"`
	Name string `json:"name"`
	Cores int `json:cores`
	Memory float32 `json:memory`
	Storage float32 `json:storage`
}

func (h *Hosting) GetHosting(db *sql.DB) error {
	statement := fmt.Sprintf("SELECT id, name, cores, memory, storage FROM HOSTING WHERE id=%d", h.Id)
	return db.QueryRow(statement).Scan(&h.Id, &h.Name, &h.Cores, &h.Memory, &h.Storage)
}

func (h *Hosting) UpdateHosting(db *sql.DB) error {
	statement := fmt.Sprintf("UPDATE hosting SET name='%s', cores=%d, memory=%f, storage=%f WHERE id=%d", h.Name, h.Cores, h.Memory, h.Storage, h.Id)
	_, err := db.Exec(statement)
	return err
}

func (h *Hosting) DeleteHosting(db *sql.DB) error {
	statement := fmt.Sprintf("DELETE FROM hosting WHERE id=%d", h.Id)
	_, err := db.Exec(statement)
	return err
}

func (h *Hosting) CreateHosting(db *sql.DB) error {
	statement := fmt.Sprintf("INSERT INTO hosting(name, cores, memory, storage) VALUES('%s', %d, %f, %f)", h.Name, h.Cores, h.Memory, h.Storage)
	_, err := db.Exec(statement)

	if err != nil {
		return err
	}

	err = db.QueryRow("SELECT last_insert_rowid();").Scan(&h.Id)

	if err != nil {
		return err
	}

	return nil
}

func GetHostings(db *sql.DB) ([]Hosting, error) {
	statement := fmt.Sprintf("SELECT id, name, cores, memory, storage FROM hosting")
	rows, err := db.Query(statement)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	hostingList := []Hosting{}

	for rows.Next() {
		var h Hosting
		if err := rows.Scan(&h.Id, &h.Name, &h.Cores, &h.Memory, &h.Storage); err != nil {
			return nil, err
		}
		hostingList = append(hostingList, h)
	}

	return hostingList, nil
}


