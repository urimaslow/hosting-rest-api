package db

import (
	"database/sql"
	"fmt"
	"hosting-rest-api/util"
	"testing"
)

func TestDatabase(t *testing.T) {
	openConnectionT()
	checkConnection()
	checkTableCreation()
	insertHosting()
	selectHosting()
	deleteAllRows()
	defer Db.Close()
}

func openConnectionT()() {
	var err error
	Db, err = sql.Open("sqlite3", ":memory:")
	util.CheckErr(err)
}

func insertHosting() {
	tx := beginTransaction()
	stmt := prepareStatement(tx)
	defer stmt.Close()
	var err error
	for i := 0; i < 4; i++ {
		_, err = stmt.Exec(i, fmt.Sprint("Test-", i), 16, 8.0, 500.0)
	}
	util.CheckErr(err)
	tx.Commit()
}

func beginTransaction()(tx *sql.Tx) {
	tx, err := Db.Begin()
	util.CheckErr(err)
	return
}

func prepareStatement(tx *sql.Tx)(stmt *sql.Stmt) {
	stmt, err := tx.Prepare("insert into HOSTING(ID, NAME, CORES, MEMORY, STORAGE) values(?, ?, ?, ?, ?)")
	util.CheckErr(err)
	return
}

func selectHosting() {
	rows, err := Db.Query("select * from HOSTING")
	util.CheckErr(err)
	printRows(rows)
	defer rows.Close()
}

func printRows(rows *sql.Rows) {
	for rows.Next() {
		var id int
		var name string
		var cores int
		var memory float32
		var storage float32
		err := rows.Scan(&id, &name, &cores, &memory, &storage)
		util.CheckErr(err)
		fmt.Printf("id=%d, name=%s, cores=%d, memory=%f, storage=%f\n", id, name, cores, memory, storage)
	}
	err := rows.Err()
	util.CheckErr(err)
}

func deleteAllRows() {
	_, err := Db.Exec("delete from HOSTING ")
	util.CheckErr(err)
}
