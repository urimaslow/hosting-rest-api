package db

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"hosting-rest-api/config"
	"hosting-rest-api/util"
)

var (
	Db *sql.DB
)

func InitDatabase() {
	openConnection()
	checkConnection()
	checkTableCreation()
}

func openConnection()() {
	var err error
	Db, err = sql.Open(config.Cfg.Database.Plugin, config.Cfg.Database.Source)
	util.CheckErr(err)
}

func checkConnection() {
	util.CheckErr(Db.Ping())
}

func checkTableCreation() {
	_, err := Db.Exec("create table if not exists HOSTING(ID integer PRIMARY KEY, NAME string not null, CORES integer, MEMORY float, STORAGE float);")
	util.CheckErr(err)
}