FROM golang:1.9.3

RUN apt-get update; apt-get install -qq make gettext-base; apt-get clean

RUN go get -u github.com/golang/dep/cmd/dep

ADD vendor /go/src

ADD . /go/src/hosting-rest-api
WORKDIR /go/src/hosting-rest-api

RUN go build -o wshosting

RUN chmod +x ./run.sh

CMD ["./run.sh"]