# file: $GOPATH/hosting-rest-api/features/handler.feature

Feature: Testing a REST API
  Users should be able to submit GET, POST, PUT and DELETE requests to a web service.

  Scenario: Data Upload to a web service
    When users upload data on a project
    Then the server should handle it and return a success status

  Scenario: Data retrieval from a web service
    When users want to get information on the Hosting project
    Then the requested data is returned

  Scenario: Data update from a web service
    When users want to update information on the Hosting project
    Then the server should handle it and return a success status

  Scenario: Data delete from a web service
    When users want to delete information on the Hosting project
    Then the server should handle it and return a success status

  Scenario: Data list retrieval from a web service
    When users want to get information list on the Hosting project
    Then the requested data is returned