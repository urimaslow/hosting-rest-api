package main

import (
	"bytes"
	"encoding/json"
	"github.com/DATA-DOG/godog"
	"hosting-rest-api/config"
	"hosting-rest-api/db"
	"hosting-rest-api/handler"
	"hosting-rest-api/model"
	"net/http/httptest"
)

var hosting model.Hosting
var resps []*httptest.ResponseRecorder

func init() {
	config.LoadConfiguration()
	db.InitDatabase()
	makeTestHosting()
}

func makeTestHosting() {
	hosting = model.Hosting{1, "New Hosting", 8, 16.0, 500.0}
}

func usersUploadDataOnAProject() error {
	v, err := json.Marshal(hosting)
	if err != nil {
		return err
	}
	w := httptest.NewRecorder()
	r := httptest.NewRequest("POST", "/hosting",bytes.NewBuffer(v))
	handler.CreateHosting(w, r)
	resps = append(resps, w)

	return nil
}

func theServerShouldHandleItAndReturnASuccessStatus() error {
	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/hosting/1", nil)
	handler.GetHosting(w, r)
	resps = append(resps, w)

	return nil
}

func usersWantToUpdateInformationOnTheHostingProject() error {
	w := httptest.NewRecorder()
	hosting.Name = "Modified hosting"
	r := httptest.NewRequest("PUT", "/hosting/1", nil)
	handler.UpdateHosting(w, r)
	resps = append(resps, w)

	return nil
}

func usersWantToDeleteInformationOnTheHostingProject() error {
	w := httptest.NewRecorder()
	r := httptest.NewRequest("DELETE", "/hosting/1", nil)
	handler.DeleteHosting(w, r)
	resps = append(resps, w)
	
	return nil
}

func usersWantToGetInformationListOnTheHostingProject() error {
	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/hostings", nil)
	handler.GetHostings(w, r)
	resps = append(resps, w)

	return nil
}

func usersWantToGetInformationOnTheHostingProject() error {
	return nil
}

func theRequestedDataIsReturned() error {
	return nil
}

func FeatureContext(s *godog.Suite) {
	s.Step(`^users upload data on a project$`, usersUploadDataOnAProject)
	s.Step(`^the server should handle it and return a success status$`, theServerShouldHandleItAndReturnASuccessStatus)
	s.Step(`^users want to get information on the Hosting project$`, usersWantToGetInformationOnTheHostingProject)
	s.Step(`^the requested data is returned$`, theRequestedDataIsReturned)
	s.Step(`^users want to update information on the Hosting project$`, usersWantToUpdateInformationOnTheHostingProject)
	s.Step(`^users want to delete information on the Hosting project$`, usersWantToDeleteInformationOnTheHostingProject)
	s.Step(`^users want to get information list on the Hosting project$`, usersWantToGetInformationListOnTheHostingProject)
}