package handler

import (
	"database/sql"
	"encoding/json"
	"github.com/gorilla/mux"
	"hosting-rest-api/config"
	"hosting-rest-api/db"
	"hosting-rest-api/model"
	"net/http"
	"strconv"
)

var (
	Router *mux.Router
)

const (
	errHostingId =  "Invalid hosting ID"
	errHostingNotFound = "Hosting not found"
	invalidRequest = "Invalid rquest"
	void = ""
)


func InitHandlers() {
	Router = mux.NewRouter()
	Router.HandleFunc("/hosting", CreateHosting).Methods("POST")
	Router.HandleFunc("/hostings", GetHostings).Methods("GET")
	Router.HandleFunc("/hosting/{hostingId}", GetHosting).Methods("GET")
	Router.HandleFunc("/hosting/{hostingId}", UpdateHosting).Methods("PUT")
	Router.HandleFunc("/hosting/{hostingId}", DeleteHosting).Methods("DELETE")

	http.ListenAndServe(":" + strconv.Itoa(config.Cfg.Port), Router)
}

func CreateHosting(w http.ResponseWriter, r *http.Request) {
	h, err := validateRequest(r)
	if err != nil {
		sendResponse(w, http.StatusBadRequest, err.Error(), nil)
		return
	}
	err = h.CreateHosting(db.Db)
	if err != nil {
		sendResponse(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	sendResponse(w, http.StatusCreated, void, h)
}

func GetHostings(w http.ResponseWriter, r *http.Request) {
	hostings, err := model.GetHostings(db.Db)
	if err != nil {
		sendResponse(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	if len(hostings) == 0 {
		sendResponse(w, http.StatusInternalServerError, void, hostings)
		return
	}
	sendResponse(w, http.StatusOK, "", hostings)
}

func GetHosting(w http.ResponseWriter, r *http.Request) {
	id, err := getHostingId(r)
	if err != nil {
		sendResponse(w, http.StatusBadRequest, errHostingId, nil)
		return
	}
	h := model.Hosting{Id: id}
	if err := h.GetHosting(db.Db); err != nil {
		switch err {
		case sql.ErrNoRows:
			sendResponse(w, http.StatusBadRequest, errHostingNotFound, nil)
		default:
			sendResponse(w, http.StatusInternalServerError, err.Error(), nil)
		}
		return
	}
	sendResponse(w, http.StatusOK, void, h)
}

func UpdateHosting(w http.ResponseWriter, r *http.Request) {

	id, err := getHostingId(r)
	if err != nil {
		sendResponse(w, http.StatusBadRequest, errHostingId, nil)
		return
	}
	h, err := validateRequest(r)
	if err != nil {
		sendResponse(w, http.StatusBadRequest, invalidRequest, nil)
		return
	}
	h.Id = id
	if err := h.UpdateHosting(db.Db); err != nil {
		sendResponse(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	sendResponse(w, http.StatusOK, void, h)
}

func DeleteHosting(w http.ResponseWriter, r *http.Request) {
	id, err := getHostingId(r)
	if err != nil {
		sendResponse(w, http.StatusBadRequest, errHostingId, nil)
		return
	}
	h := model.Hosting{Id: id}
	if err := h.DeleteHosting(db.Db); err != nil {
		sendResponse(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	sendResponse(w, http.StatusOK, void, map[string]string{"result": "success"})
}

func getHostingId(r *http.Request)(id int, err error) {
	vars := mux.Vars(r)
	id, err = strconv.Atoi(vars["hostingId"])
	return id, err
}

func validateRequest(r *http.Request)(h model.Hosting, err error) {
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()
	err = decoder.Decode(&h)
	return h,err
}

func sendResponse(w http.ResponseWriter, status int, err string, i interface{}) {
	if err != "" {
		i = map[string]string{"error": err}
	}
	rs, _ := json.Marshal(i)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(rs)
}
